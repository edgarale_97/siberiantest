﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace TestApiSiberian.Domain.Common
{
    public abstract class AuditableBaseEntityRestaurante
    {
        [Column("IDRestaurante")]
        public virtual int Id { get; set; }
        public DateTime FechaCreacion { get; set; }
    }
}
