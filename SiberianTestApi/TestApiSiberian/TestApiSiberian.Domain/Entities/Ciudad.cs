﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Domain.Common;

namespace TestApiSiberian.Domain.Entities
{
    public class Ciudad : AuditableBaseEntityCiudad
    {
        public string NombreCiudad { get; set; }
    }
}
