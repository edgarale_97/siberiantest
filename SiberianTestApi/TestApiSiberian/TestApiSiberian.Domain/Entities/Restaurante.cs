﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Domain.Common;

namespace TestApiSiberian.Domain.Entities
{
    public class Restaurante : AuditableBaseEntityRestaurante
    {
        public string NombreRestaurante { get; set; }
        public int IDCiudad { get; set; }
        public int NumeroAforo { get; set; }
        public string Telefono { get; set; }
        
    }
}
