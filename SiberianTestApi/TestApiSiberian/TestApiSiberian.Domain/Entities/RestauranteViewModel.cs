﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Domain.Common;

namespace TestApiSiberian.Domain.Entities
{
    public class RestauranteViewModel : AuditableBaseEntityRestaurante
    {
        public string NombreRestaurante { get; set; }
        public string NombreCiudad { get; set; }
        public int NumeroAforo { get; set; }
        public string Telefono { get; set; }
        
    }
}
