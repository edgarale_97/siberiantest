﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Restaurants.Commands.UpdateRestaurant
{
    public class UpdateRestaurantCommand : IRequest<Response<int>>
    {
        public int IDRestaurante { get; set; }
        public string NombreRestaurante { get; set; }
        public int NumeroAforo { get; set; }
        public int IDCiudad { get; set; }
        public string Telefono { get; set; }

        public class UpdateRestaurantCommandHandler : IRequestHandler<UpdateRestaurantCommand, Response<int>>
        {
            private readonly IRestauranteRepositoryAsync _restauranteRepository;
            public UpdateRestaurantCommandHandler(IRestauranteRepositoryAsync productRepository)
            {
                _restauranteRepository = productRepository;
            }
            public async Task<Response<int>> Handle(UpdateRestaurantCommand command, CancellationToken cancellationToken)
            {
                var restaurante = await _restauranteRepository.GetByIdAsync(command.IDRestaurante);

                if (restaurante == null)
                {
                    throw new ApiException($"Product Not Found.");
                }
                else
                {
                    restaurante.NombreRestaurante = command.NombreRestaurante;
                    restaurante.IDCiudad = command.IDCiudad;
                    restaurante.NumeroAforo = command.NumeroAforo;
                    restaurante.Telefono = command.Telefono;
                    await _restauranteRepository.UpdateAsync(restaurante);
                    return new Response<int>(restaurante.Id);
                }
            }
        }
    }
}
