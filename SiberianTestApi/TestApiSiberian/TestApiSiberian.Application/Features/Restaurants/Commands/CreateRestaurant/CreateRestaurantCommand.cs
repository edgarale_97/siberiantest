﻿using AutoMapper;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Features.Restaurants.Commands.CreateRestaurant
{
    public partial class CreateRestaurantCommand : IRequest<Response<int>>
    {
        public string NombreRestaurante { get; set; }
        public int IDCiudad { get; set; }
        public int NumeroAforo { get; set; }
        public string Telefono { get; set; }
    }
    public class CreateRestaurantCommandHandler : IRequestHandler<CreateRestaurantCommand, Response<int>>
    {
        private readonly IRestauranteRepositoryAsync _resaturanteRepository;
        private readonly IMapper _mapper;
        public CreateRestaurantCommandHandler(IRestauranteRepositoryAsync restauranteRepository, IMapper mapper)
        {
            _resaturanteRepository = restauranteRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateRestaurantCommand request, CancellationToken cancellationToken)
        {
            var restaurante = _mapper.Map<Restaurante>(request);
            await _resaturanteRepository.AddAsync(restaurante);
            return new Response<int>(restaurante.Id);
        }
    }
}
