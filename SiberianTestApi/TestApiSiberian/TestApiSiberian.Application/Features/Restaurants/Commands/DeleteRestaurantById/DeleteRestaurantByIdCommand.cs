﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Restaurants.Commands.DeleteRestaurantById
{
    public class DeleteRestaurantByIdCommand : IRequest<Response<int>>
    {
        public int IDRestaurante { get; set; }
        public class DeleteRestaurantByIdCommandHandler : IRequestHandler<DeleteRestaurantByIdCommand, Response<int>>
        {
            private readonly IRestauranteRepositoryAsync _restaruanteRepository;
            public DeleteRestaurantByIdCommandHandler(IRestauranteRepositoryAsync restauranteRepository)
            {
                _restaruanteRepository = restauranteRepository;
            }
            public async Task<Response<int>> Handle(DeleteRestaurantByIdCommand command, CancellationToken cancellationToken)
            {
                var restaurante = await _restaruanteRepository.GetByIdAsync(command.IDRestaurante);
                if (restaurante == null) throw new ApiException($"Restaurant Not Found.");
                await _restaruanteRepository.DeleteAsync(restaurante);
                return new Response<int>(restaurante.Id);
            }
        }
    }
}
