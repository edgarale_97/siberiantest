﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.GetAllRestaurants
{
    public class GetAllRestaurantsViewModel
    {
        public int Id { get; set; }
        public int IDCiudad { get; set; }
        public string NombreRestaurante { get; set; }
        public int NumeroAforo { get; set; }
        public string Telefono { get; set; }
    }
}
