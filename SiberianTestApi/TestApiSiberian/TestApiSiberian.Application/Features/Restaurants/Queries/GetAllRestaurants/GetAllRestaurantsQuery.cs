﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Filters;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.GetAllRestaurants
{
    public class GetAllRestaurantsQuery : IRequest<PagedResponse<IEnumerable<GetAllRestaurantsViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllRestaurantsQueryHandler : IRequestHandler<GetAllRestaurantsQuery, PagedResponse<IEnumerable<GetAllRestaurantsViewModel>>>
    {
        private readonly IRestauranteRepositoryAsync _restauranteRepository;
        private readonly IMapper _mapper;
        public GetAllRestaurantsQueryHandler(IRestauranteRepositoryAsync restauranteRepository, IMapper mapper)
        {
            _restauranteRepository = restauranteRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetAllRestaurantsViewModel>>> Handle(GetAllRestaurantsQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllRestaurantsParameter>(request);
            var restaurante = await _restauranteRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var resaturanteViewModel = _mapper.Map<IEnumerable<GetAllRestaurantsViewModel>>(restaurante);
            return new PagedResponse<IEnumerable<GetAllRestaurantsViewModel>>(resaturanteViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
