﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Filters;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.ExecuteSpRestauranesQuery
{
    public class GetSpRestauranesQuery : IRequest<PagedResponse<IEnumerable<GetSpRestauranesViewModel>>>
    {
        public int IDRestaurante { get; set; }
        public string NombreRestaurante { get; set; }
        public int NumeroAforo { get; set; }
        public int IDCiudad { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetSpRestauranesQueryHandler : IRequestHandler<GetSpRestauranesQuery, PagedResponse<IEnumerable<GetSpRestauranesViewModel>>>
    {
        private readonly IRestauranteRepositoryAsync _restauranteRepository;
        private readonly IMapper _mapper;
        public GetSpRestauranesQueryHandler(IRestauranteRepositoryAsync restauranteRepository, IMapper mapper)
        {
            _restauranteRepository = restauranteRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetSpRestauranesViewModel>>> Handle(GetSpRestauranesQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetSpRestauranesParameter>(request);
            var query = $"CALL Sp_Restauranes(\"{validFilter.Tipo}\",{validFilter.IDRestaurante},\"{validFilter.NombreRestaurante}\"," +
                $"{validFilter.IDCiudad},{validFilter.NumeroAforo},\"{validFilter.Telefono}\",curdate());";
            if(validFilter.Tipo != "v")
            {
                var restaurante = await _restauranteRepository.ExecuteSpRestauranesAsync(query);
                if (restaurante == null) throw new ApiException($"An error ocurred in Sp_Restauranes.");
                var restauranteViewModel = _mapper.Map<IEnumerable<GetSpRestauranesViewModel>>(restaurante);
                return new PagedResponse<IEnumerable<GetSpRestauranesViewModel>>(restauranteViewModel, validFilter.PageNumber, validFilter.PageSize);
            }
            else
            {
                var restaurante = await _restauranteRepository.ExecuteSpDeleteRestauranesAsync(query);
                if (restaurante == null) throw new ApiException($"An error ocurred in Sp_Restauranes.");
                var restauranteViewModel = _mapper.Map<IEnumerable<GetSpDeleteRestauranesViewModel>>(restaurante);
                return new PagedResponse<IEnumerable<GetSpRestauranesViewModel>>(_mapper.Map<IEnumerable<GetSpRestauranesViewModel>>(restauranteViewModel), validFilter.PageNumber, validFilter.PageSize);
            }
        }
    }
}
