﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.ExecuteSpRestauranesQuery
{
    public class GetSpRestauranesViewModel
    {
        public int Id { get; set; }
        public string NombreRestaurante { get; set; }
        public string NombreCiudad { get; set; }
        public int NumeroAforo { get; set; }
        public string Telefono { get; set; }
    }
}
