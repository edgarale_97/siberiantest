﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Application.Filters;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.ExecuteSpRestauranesQuery
{
    public class GetSpRestauranesParameter : RequestParameter
    {
        public int IDRestaurante { get; set; }
        public string NombreRestaurante { get; set; }
        public int NumeroAforo { get; set; }
        public int IDCiudad { get; set; }
        public string Telefono { get; set; }
        public string Tipo { get; set; }
    }
}
