﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Features.Restaurants.Queries.GetRestaurantById
{
    public class GetRestaurantByIdQuery : IRequest<Response<Restaurante>>
    {
        public int IDRestaurante { get; set; }
        public class GetRestaurantByIdQueryHandler : IRequestHandler<GetRestaurantByIdQuery, Response<Restaurante>>
        {
            private readonly IRestauranteRepositoryAsync _restauranteRepository;
            public GetRestaurantByIdQueryHandler(IRestauranteRepositoryAsync restauranteRepository)
            {
                _restauranteRepository = restauranteRepository;
            }
            public async Task<Response<Restaurante>> Handle(GetRestaurantByIdQuery query, CancellationToken cancellationToken)
            {
                var restaurante = await _restauranteRepository.GetByIdAsync(query.IDRestaurante);
                if (restaurante == null) throw new ApiException($"Restaurant Not Found.");
                return new Response<Restaurante>(restaurante);
            }
        }
    }
}
