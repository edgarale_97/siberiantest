﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Filters;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Queries.ExecuteSpProductosQuery
{
    public class GetSpProductosQuery : IRequest<PagedResponse<IEnumerable<GetSpProductosViewModel>>>
    {
        public int IDEmpresa { get; set; }
        public int IDProducto { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }

    }
    public class GetSpProductosQueryHandler : IRequestHandler<GetSpProductosQuery, PagedResponse<IEnumerable<GetSpProductosViewModel>>>
    {
        private readonly IProductSiberianRepositoryAsync _productosRepository;
        private readonly IMapper _mapper;
        public GetSpProductosQueryHandler(IProductSiberianRepositoryAsync productRepository, IMapper mapper)
        {
            _productosRepository = productRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetSpProductosViewModel>>> Handle(GetSpProductosQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetSpProductosParameter>(request);
            var query = $"CALL sp_ConsultaProductoID(\"{validFilter.IDEmpresa}\",{validFilter.IDProducto});";
            var producto = await _productosRepository.ExecuteSpProductosAsync(query);
            if (producto == null) throw new ApiException($"An error ocurred in sp_ConsultaProductoID.");
            var restauranteViewModel = _mapper.Map<IEnumerable<GetSpProductosViewModel>>(producto);
            return new PagedResponse<IEnumerable<GetSpProductosViewModel>>(restauranteViewModel, validFilter.PageNumber, validFilter.PageSize);
            
        }
    }
}
