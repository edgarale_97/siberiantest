﻿using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Application.Filters;

namespace TestApiSiberian.Application.Features.Queries.ExecuteSpProductosQuery
{
    public class GetSpProductosParameter : RequestParameter
    {
        public int IDEmpresa { get; set; }
        public int IDProducto { get; set; }
      
    }
}
