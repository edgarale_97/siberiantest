﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Features.Queries.ExecuteSpProductosQuery
{
    public class GetSpProductosViewModel
    {
        public int? ID { get; set; }
        public int? empresa { get; set; }
        public int? categoriaId { get; set; }
        public string? categoriaNombre { get; set; }
        public int? empresa_aso { get; set; }
        public string? Nombre { get; set; }
        public string? Descripcion { get; set; }
        public string? cod_barras { get; set; }
        public string? imagen { get; set; }
        public int? Stock { get; set; }
        public decimal? Precio { get; set; }
        public decimal? precioiva { get; set; }
        public decimal? preciofinal { get; set; }
        public long? descuento { get; set; }
        public decimal? iva { get; set; }
        public string? estado { get; set; }
        public string? marca { get; set; }
        public string? des_marca { get; set; }
        public decimal? peso { get; set; }
        public decimal? altura { get; set; }
        public decimal? ancho { get; set; }
        public decimal? profundidad { get; set; }
        public decimal? volumen { get; set; }
        public string? cod_ext { get; set; }
        public string? SKU { get; set; }
        public string? Variante { get; set; }
        public int? unidxcaja { get; set; }
        public string? DescntTipo { get; set; }
        public string? DescntValor { get; set; }
        public string? DescntPorct { get; set; }
        public string? DescntFDesde { get; set; }
        public string? DescntFHasta { get; set; }
        public DateTime? fchcreacion { get; set; }
        public DateTime? fchmodifica { get; set; }
        public int? orden { get; set; }
        public int? autoestado { get; set; }
        public string? pt_validaciones { get; set; }
    }
}
