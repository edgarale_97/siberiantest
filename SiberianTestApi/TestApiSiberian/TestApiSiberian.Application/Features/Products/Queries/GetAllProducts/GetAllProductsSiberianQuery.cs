﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Filters;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Products.Queries.GetAllProducts
{
    public class GetAllProductsSiberianQuery : IRequest<PagedResponse<IEnumerable<GetAllProductsSiberianViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllProductsSiberianQueryHandler : IRequestHandler<GetAllProductsSiberianQuery, PagedResponse<IEnumerable<GetAllProductsSiberianViewModel>>>
    {
        private readonly IProductSiberianRepositoryAsync _productRepository;
        private readonly IMapper _mapper;
        public GetAllProductsSiberianQueryHandler(IProductSiberianRepositoryAsync productRepository, IMapper mapper)
        {
            _productRepository = productRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetAllProductsSiberianViewModel>>> Handle(GetAllProductsSiberianQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllProductsSiberianParameter>(request);
            var product = await _productRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var productViewModel = _mapper.Map<IEnumerable<GetAllProductsSiberianViewModel>>(product);
            return new PagedResponse<IEnumerable<GetAllProductsSiberianViewModel>>(productViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
