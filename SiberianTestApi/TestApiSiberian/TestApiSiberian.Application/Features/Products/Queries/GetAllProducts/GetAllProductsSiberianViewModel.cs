﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Features.Products.Queries.GetAllProducts
{
    public class GetAllProductsSiberianViewModel
    {
        public int pt_id { get; set; }
        public int pt_empresa_asociada { get; set; }
        public string pt_tipo { get; set; }
        public string pt_nombre { get; set; }
        public string pt_descripcion { get; set; }
        public string pt_cod_barras { get; set; }
        public string pt_imagen { get; set; }
        public decimal pt_stock { get; set; }
        public decimal pt_iva { get; set; }
        public string pt_estado { get; set; }
        public string pt_marca { get; set; }
        public decimal pt_altura { get; set; }
        public decimal pt_ancho { get; set; }
        public decimal pt_profundidad { get; set; }
        public decimal pt_volumen { get; set; }
        public string pt_cod_ext { get; set; }
        public string pt_SKU { get; set; }
        public string pt_tipo_variante { get; set; }
        public string pt_unidxcaja { get; set; }
        public string pt_cod_proveedor { get; set; }
        public string pt_dia_restringido { get; set; }
        public string pt_hora_fin_restringido { get; set; }
        public string pt_hora_inicio_restringido { get; set; }
        public string pt_nuevo { get; set; }
        public string pt_act_nombre { get; set; }
        public string pt_act_marca { get; set; }
        public string pt_fchcreacion { get; set; }
        public string pt_usrcreacion { get; set; }
        public string pt_fchmodifica { get; set; }
        public string pt_usrmodifica { get; set; }
        public string pt_sort_order { get; set; }
        public string pt_autoestado { get; set; }
        public string IDContifico { get; set; }
        public string pt_titulo_video { get; set; }
        public string pt_subtitulo_video { get; set; }
        public string pt_urlvideo_video { get; set; }
        public string pt_excluir_matrix { get; set; }
        public string pt_token { get; set; }
        public string pt_IDOldSE { get; set; }
        public string pt_Codigo { get; set; }
        public string pt_validaciones { get; set; }
        public string CodigoSap { get; set; }
    }
}
