﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Features.Ciudades.Queries.GetCiudadById
{
    public class GetCiudadByIdQuery : IRequest<Response<Ciudad>>
    {
        public int IDCiudad { get; set; }
        public class GetCiudadByIdQueryHandler : IRequestHandler<GetCiudadByIdQuery, Response<Ciudad>>
        {
            private readonly ICiudadRepositoryAsync _ciudadRepository;
            public GetCiudadByIdQueryHandler(ICiudadRepositoryAsync ciudadRepository)
            {
                _ciudadRepository = ciudadRepository;
            }
            public async Task<Response<Ciudad>> Handle(GetCiudadByIdQuery query, CancellationToken cancellationToken)
            {
                var ciudad = await _ciudadRepository.GetByIdAsync(query.IDCiudad);
                if (ciudad == null) throw new ApiException($"City Not Found.");
                return new Response<Ciudad>(ciudad);
            }
        }
    }
}
