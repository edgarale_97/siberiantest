﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Features.Ciudades.Queries.GetAllCiudades
{
    public class GetAllCiudadesViewModel
    {
        public int Id { get; set; }
        public string NombreCiudad { get; set; }
        
    }
}
