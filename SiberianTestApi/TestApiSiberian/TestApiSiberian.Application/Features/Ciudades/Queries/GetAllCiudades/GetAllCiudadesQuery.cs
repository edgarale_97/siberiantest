﻿using AutoMapper;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Filters;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Ciudades.Queries.GetAllCiudades
{
    public class GetAllCiudadesQuery : IRequest<PagedResponse<IEnumerable<GetAllCiudadesViewModel>>>
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
    public class GetAllCiudadesQueryHandler : IRequestHandler<GetAllCiudadesQuery, PagedResponse<IEnumerable<GetAllCiudadesViewModel>>>
    {
        private readonly ICiudadRepositoryAsync _ciudadRepository;
        private readonly IMapper _mapper;
        public GetAllCiudadesQueryHandler(ICiudadRepositoryAsync ciudadRepository, IMapper mapper)
        {
            _ciudadRepository = ciudadRepository;
            _mapper = mapper;
        }

        public async Task<PagedResponse<IEnumerable<GetAllCiudadesViewModel>>> Handle(GetAllCiudadesQuery request, CancellationToken cancellationToken)
        {
            var validFilter = _mapper.Map<GetAllCiudadesParameter>(request);
            var ciudad = await _ciudadRepository.GetPagedReponseAsync(validFilter.PageNumber, validFilter.PageSize);
            var ciudadViewModel = _mapper.Map<IEnumerable<GetAllCiudadesViewModel>>(ciudad);
            return new PagedResponse<IEnumerable<GetAllCiudadesViewModel>>(ciudadViewModel, validFilter.PageNumber, validFilter.PageSize);
        }
    }
}
