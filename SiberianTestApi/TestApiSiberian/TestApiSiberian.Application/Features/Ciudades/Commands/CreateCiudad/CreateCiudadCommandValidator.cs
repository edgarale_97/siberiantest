﻿using FluentValidation;
using Microsoft.EntityFrameworkCore.Internal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Features.Ciudades.Commands.CreateCiudad
{
    public class CreateCiudadCommandValidator : AbstractValidator<CreateCiudadCommand>
    {
        private readonly ICiudadRepositoryAsync ciudadRepository;

        public CreateCiudadCommandValidator(ICiudadRepositoryAsync ciudadRepository)
        {
            this.ciudadRepository = ciudadRepository;

            RuleFor(c => c.NombreCiudad)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .NotNull()
                .MaximumLength(150).WithMessage("{PropertyName} must not exceed 150 characters.");

        }

        //private async Task<bool> IsUniqueBarcode(string barcode, CancellationToken cancellationToken)
        //{
        //    return await productRepository.IsUniqueBarcodeAsync(barcode);
        //}
    }
}
