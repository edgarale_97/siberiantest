﻿using AutoMapper;
using MediatR;
using System;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Features.Ciudades.Commands.CreateCiudad
{
    public partial class CreateCiudadCommand : IRequest<Response<int>>
    {
        public string NombreCiudad { get; set; }
    }
    public class CreateCiudadCommandHandler : IRequestHandler<CreateCiudadCommand, Response<int>>
    {
        private readonly ICiudadRepositoryAsync _ciudadRepository;
        private readonly IMapper _mapper;
        public CreateCiudadCommandHandler(ICiudadRepositoryAsync ciudadRepository, IMapper mapper)
        {
            _ciudadRepository = ciudadRepository;
            _mapper = mapper;
        }

        public async Task<Response<int>> Handle(CreateCiudadCommand request, CancellationToken cancellationToken)
        {
            var ciudad = _mapper.Map<Ciudad>(request);
            await _ciudadRepository.AddAsync(ciudad);
            return new Response<int>(ciudad.Id);
        }
    }
}
