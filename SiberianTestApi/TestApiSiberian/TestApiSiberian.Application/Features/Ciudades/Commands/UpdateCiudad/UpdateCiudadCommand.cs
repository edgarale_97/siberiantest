﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Ciudades.Commands.UpdateCiudad
{
    public class UpdateCiudadCommand : IRequest<Response<int>>
    {
        public int IDCiudad { get; set; }
        public string NombreCiudad { get; set; }
        
        public class UpdateCiudadCommandHandler : IRequestHandler<UpdateCiudadCommand, Response<int>>
        {
            private readonly ICiudadRepositoryAsync _ciudadRepository;
            public UpdateCiudadCommandHandler(ICiudadRepositoryAsync ciudadRepository)
            {
                _ciudadRepository = ciudadRepository;
            }
            public async Task<Response<int>> Handle(UpdateCiudadCommand command, CancellationToken cancellationToken)
            {
                var ciudad = await _ciudadRepository.GetByIdAsync(command.IDCiudad);

                if (ciudad == null)
                {
                    throw new ApiException($"City Not Found.");
                }
                else
                {
                    ciudad.NombreCiudad = command.NombreCiudad;
                    await _ciudadRepository.UpdateAsync(ciudad);
                    return new Response<int>(ciudad.Id);
                }
            }
        }
    }
}
