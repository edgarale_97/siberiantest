﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TestApiSiberian.Application.Exceptions;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Application.Wrappers;

namespace TestApiSiberian.Application.Features.Ciudades.Commands.DeleteCiudadById
{
    public class DeleteCiudadByIdCommand : IRequest<Response<int>>
    {
        public int IDCiudad { get; set; }
        public class DeleteCiudadByIdCommandHandler : IRequestHandler<DeleteCiudadByIdCommand, Response<int>>
        {
            private readonly ICiudadRepositoryAsync _ciudadRepository;
            public DeleteCiudadByIdCommandHandler(ICiudadRepositoryAsync ciudadRepository)
            {
                _ciudadRepository = ciudadRepository;
            }
            public async Task<Response<int>> Handle(DeleteCiudadByIdCommand command, CancellationToken cancellationToken)
            {
                var ciudad = await _ciudadRepository.GetByIdAsync(command.IDCiudad);
                if (ciudad == null) throw new ApiException($"City Not Found.");
                await _ciudadRepository.DeleteAsync(ciudad);
                return new Response<int>(ciudad.Id);
            }
        }
    }
}
