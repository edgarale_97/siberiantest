﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Interfaces
{
    public interface IGenericRepositoryAsync<T> where T : class
    {
        Task<T> GetByIdAsync(int id);
        Task<IReadOnlyList<T>> GetAllAsync();
        Task<IReadOnlyList<T>> GetPagedReponseAsync(int pageNumber, int pageSize);
        Task<T> AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task<List<RestauranteViewModel>> ExecuteSpRestauranesAsync(string query);
        Task<List<ProductosViewModel>> ExecuteSpProductosAsync(string query);
        Task<List<DeleteRestauranteViewModel>> ExecuteSpDeleteRestauranesAsync(string query);
    }
}
