﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Application.DTOs.Email;

namespace TestApiSiberian.Application.Interfaces
{
    public interface IEmailService
    {
        Task SendAsync(EmailRequest request);
    }
}
