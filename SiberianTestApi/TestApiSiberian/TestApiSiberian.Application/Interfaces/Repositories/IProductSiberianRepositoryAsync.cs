﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Interfaces.Repositories
{
    public interface IProductSiberianRepositoryAsync : IGenericRepositoryAsync<ProductosSiberian>
    {
        Task<bool> IsUniqueBarcodeAsync(string barcode);
    }
}
