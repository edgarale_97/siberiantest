﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestApiSiberian.Application.Interfaces
{
    public interface IAuthenticatedUserService
    {
        string UserId { get; }
    }
}
