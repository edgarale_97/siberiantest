﻿using AutoMapper;
using TestApiSiberian.Application.Features.Ciudades.Commands.CreateCiudad;
using TestApiSiberian.Application.Features.Ciudades.Queries.GetAllCiudades;
using TestApiSiberian.Application.Features.Products.Commands.CreateProduct;
using TestApiSiberian.Application.Features.Products.Queries.GetAllProducts;
using TestApiSiberian.Application.Features.Queries.ExecuteSpProductosQuery;
using TestApiSiberian.Application.Features.Restaurants.Commands.CreateRestaurant;
using TestApiSiberian.Application.Features.Restaurants.Queries.ExecuteSpRestauranesQuery;
using TestApiSiberian.Application.Features.Restaurants.Queries.GetAllRestaurants;
using TestApiSiberian.Domain.Entities;

namespace TestApiSiberian.Application.Mappings
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<Product, GetAllProductsViewModel>().ReverseMap();
            CreateMap<ProductosSiberian, GetAllProductsSiberianViewModel>().ReverseMap();
            CreateMap<CreateProductCommand, Product>();
            CreateMap<GetAllProductsQuery, GetAllProductsParameter>();
            CreateMap<GetAllProductsSiberianQuery, GetAllProductsSiberianParameter>();
            CreateMap<Restaurante, GetAllRestaurantsViewModel>().ReverseMap();
            CreateMap<CreateRestaurantCommand, Restaurante>();
            CreateMap<GetAllRestaurantsQuery, GetAllRestaurantsParameter>();
            CreateMap<Ciudad, GetAllCiudadesViewModel>().ReverseMap();
            CreateMap<CreateCiudadCommand, Ciudad>();
            CreateMap<GetAllCiudadesQuery, GetAllCiudadesParameter>();
            CreateMap<GetSpRestauranesQuery, GetSpRestauranesParameter>();
            CreateMap<GetSpProductosQuery, GetSpProductosParameter>();
            CreateMap<GetSpDeleteRestauranesViewModel, GetSpRestauranesViewModel>()
                .ForMember(dest => dest.Id, m => m.MapFrom(src => src.Id));
            CreateMap<RestauranteViewModel, GetSpRestauranesViewModel>().ReverseMap();
            CreateMap<ProductosViewModel, GetSpProductosViewModel>().ReverseMap();
            CreateMap<DeleteRestauranteViewModel, GetSpDeleteRestauranesViewModel>().ReverseMap();

        }
    }
}
