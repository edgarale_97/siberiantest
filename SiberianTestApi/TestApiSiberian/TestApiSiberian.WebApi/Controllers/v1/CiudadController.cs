﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApiSiberian.Application.Features.Ciudades.Commands;
using TestApiSiberian.Application.Features.Ciudades.Commands.CreateCiudad;
using TestApiSiberian.Application.Features.Ciudades.Commands.DeleteCiudadById;
using TestApiSiberian.Application.Features.Ciudades.Commands.UpdateCiudad;
using TestApiSiberian.Application.Features.Ciudades.Queries.GetAllCiudades;
using TestApiSiberian.Application.Features.Ciudades.Queries.GetCiudadById;
using TestApiSiberian.Application.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestApiSiberian.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class CiudadController : BaseApiController
    {
       
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetAllCiudadesParameter filter)
        {

            return Ok(await Mediator.Send(new GetAllCiudadesQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber }));
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetCiudadByIdQuery { IDCiudad = id }));
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CreateCiudadCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, UpdateCiudadCommand command)
        {
            if (id != command.IDCiudad)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteCiudadByIdCommand { IDCiudad = id }));
        }
    }
}
