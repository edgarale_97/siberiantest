﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApiSiberian.Application.Features.Restaurants.Commands;
using TestApiSiberian.Application.Features.Restaurants.Commands.CreateRestaurant;
using TestApiSiberian.Application.Features.Restaurants.Commands.DeleteRestaurantById;
using TestApiSiberian.Application.Features.Restaurants.Commands.UpdateRestaurant;
using TestApiSiberian.Application.Features.Restaurants.Queries.GetAllRestaurants;
using TestApiSiberian.Application.Features.Restaurants.Queries.ExecuteSpRestauranesQuery;
using TestApiSiberian.Application.Features.Restaurants.Queries.GetRestaurantById;
using TestApiSiberian.Application.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestApiSiberian.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class RestauranteController : BaseApiController
    {
       
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetAllRestaurantsParameter filter)
        {

            return Ok(await Mediator.Send(new GetAllRestaurantsQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber }));
        }

        [HttpPost("ExecuteSp")]
        public async Task<IActionResult> ExecuteSpRestauranes(GetSpRestauranesParameter executeSpRestauranesParameters)
        {

            return Ok(await Mediator.Send(new GetSpRestauranesQuery() {  IDCiudad = executeSpRestauranesParameters.IDCiudad,
            IDRestaurante = executeSpRestauranesParameters.IDRestaurante, NombreRestaurante = executeSpRestauranesParameters.NombreRestaurante,
            NumeroAforo = executeSpRestauranesParameters.NumeroAforo, PageNumber = executeSpRestauranesParameters.PageNumber, PageSize = executeSpRestauranesParameters.PageSize,
            Telefono = executeSpRestauranesParameters.Telefono, Tipo = executeSpRestauranesParameters.Tipo}));
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetRestaurantByIdQuery { IDRestaurante = id }));
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CreateRestaurantCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, UpdateRestaurantCommand command)
        {
            if (id != command.IDRestaurante)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteRestaurantByIdCommand { IDRestaurante = id }));
        }
    }
}
