﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApiSiberian.Application.Features.Products.Commands;
using TestApiSiberian.Application.Features.Products.Commands.CreateProduct;
using TestApiSiberian.Application.Features.Products.Commands.DeleteProductById;
using TestApiSiberian.Application.Features.Products.Commands.UpdateProduct;
using TestApiSiberian.Application.Features.Products.Queries.GetAllProducts;
using TestApiSiberian.Application.Features.Products.Queries.GetProductById;
using TestApiSiberian.Application.Features.Queries.ExecuteSpProductosQuery;
using TestApiSiberian.Application.Filters;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestApiSiberian.WebApi.Controllers.v1
{
    [ApiVersion("1.0")]
    public class ProductController : BaseApiController
    {
       
        // GET: api/<controller>
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] GetAllProductsSiberianParameter filter)
        {

            return Ok(await Mediator.Send(new GetAllProductsSiberianQuery() { PageSize = filter.PageSize, PageNumber = filter.PageNumber }));
        }

        [HttpPost("ProductById")]
        public async Task<IActionResult> ProductById(GetSpProductosParameter filter)
        {

            return Ok(await Mediator.Send(new GetSpProductosQuery() { IDEmpresa = filter.IDEmpresa, IDProducto = filter.IDProducto, PageNumber = filter.PageNumber, PageSize = filter.PageSize }));
        }

        // GET api/<controller>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(await Mediator.Send(new GetProductByIdQuery { Id = id }));
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post(CreateProductCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        // PUT api/<controller>/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, UpdateProductCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }
            return Ok(await Mediator.Send(command));
        }

        // DELETE api/<controller>/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            return Ok(await Mediator.Send(new DeleteProductByIdCommand { Id = id }));
        }
    }
}
