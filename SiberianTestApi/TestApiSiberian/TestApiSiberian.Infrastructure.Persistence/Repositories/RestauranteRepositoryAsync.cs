﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Domain.Entities;
using TestApiSiberian.Infrastructure.Persistence.Contexts;
using TestApiSiberian.Infrastructure.Persistence.Repository;

namespace TestApiSiberian.Infrastructure.Persistence.Repositories
{
    public class RestauranteRepositoryAsync : GenericRepositoryAsync<Restaurante>, IRestauranteRepositoryAsync
    {
        private readonly DbSet<Restaurante> _restaurantes;

        public RestauranteRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _restaurantes = dbContext.Set<Restaurante>();
        }

        //public Task<bool> IsUniqueBarcodeAsync(string barcode)
        //{
        //    return _products
        //        .AllAsync(p => p.Barcode != barcode);
        //}
    }
}
