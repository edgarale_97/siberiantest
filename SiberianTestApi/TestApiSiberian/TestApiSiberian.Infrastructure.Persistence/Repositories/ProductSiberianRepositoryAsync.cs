﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Domain.Entities;
using TestApiSiberian.Infrastructure.Persistence.Contexts;
using TestApiSiberian.Infrastructure.Persistence.Repository;

namespace TestApiSiberian.Infrastructure.Persistence.Repositories
{
    public class ProductSiberianRepositoryAsync : GenericRepositoryAsync<ProductosSiberian>, IProductSiberianRepositoryAsync
    {
        private readonly DbSet<ProductosSiberian> _products;

        public ProductSiberianRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _products = dbContext.Set<ProductosSiberian>();
        }

        public Task<bool> IsUniqueBarcodeAsync(string barcode)
        {
            return _products
                .AllAsync(p => p.pt_cod_ext != barcode);
        }
    }
}
