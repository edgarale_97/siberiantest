﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Domain.Entities;
using TestApiSiberian.Infrastructure.Persistence.Contexts;
using TestApiSiberian.Infrastructure.Persistence.Repository;

namespace TestApiSiberian.Infrastructure.Persistence.Repositories
{
    public class CiudadRepositoryAsync : GenericRepositoryAsync<Ciudad>, ICiudadRepositoryAsync
    {
        private readonly DbSet<Ciudad> _ciudades;

        public CiudadRepositoryAsync(ApplicationDbContext dbContext) : base(dbContext)
        {
            _ciudades = dbContext.Set<Ciudad>();
        }

        //public Task<bool> IsUniqueBarcodeAsync(string barcode)
        //{
        //    return _products
        //        .AllAsync(p => p.Barcode != barcode);
        //}
    }
}
