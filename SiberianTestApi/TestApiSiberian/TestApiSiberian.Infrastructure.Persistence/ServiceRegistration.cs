﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TestApiSiberian.Application.Interfaces;
using TestApiSiberian.Application.Interfaces.Repositories;
using TestApiSiberian.Infrastructure.Persistence.Contexts;
using TestApiSiberian.Infrastructure.Persistence.Repositories;
using TestApiSiberian.Infrastructure.Persistence.Repository;

namespace TestApiSiberian.Infrastructure.Persistence
{
    public static class ServiceRegistration
    {
        public static void AddPersistenceInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            if (configuration.GetValue<bool>("UseInMemoryDatabase"))
            {
                services.AddDbContext<ApplicationDbContext>(options =>
                    options.UseInMemoryDatabase("ApplicationDb"));
            }
            else
            {
                services.AddDbContext<ApplicationDbContext>(options =>
               options.UseMySql(
                   configuration.GetConnectionString("DefaultConnection"),
                   b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)));
            }
            #region Repositories
            services.AddTransient(typeof(IGenericRepositoryAsync<>), typeof(GenericRepositoryAsync<>));
            services.AddTransient<IProductRepositoryAsync, ProductRepositoryAsync>();
            services.AddTransient<IProductSiberianRepositoryAsync, ProductSiberianRepositoryAsync>();
            services.AddTransient<ICiudadRepositoryAsync, CiudadRepositoryAsync>();
            services.AddTransient<IRestauranteRepositoryAsync, RestauranteRepositoryAsync>();

            #endregion
        }
    }
}
