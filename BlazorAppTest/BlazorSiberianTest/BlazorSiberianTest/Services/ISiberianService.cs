﻿using BlazorSiberianTest.Models;
using BlazorSiberianTest.Models.Restaurante;
using BlazorSiberianTest.Models.Ciudad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Services
{
    public interface ISiberianService
    {
        Task<IEnumerable<CiudadData>> GetAllCiudades();
        Task<IEnumerable<RestauranteData>> GetAllRestaurantes();
        Task<RestauranteData> GetRestaurante(int id);
        Task<IEnumerable<RestauranteData>> PostRestaurante(int idCiudad, string nombreRestaurante, int numeroAforo, string telefono);
        Task<IEnumerable<RestauranteData>> PutRestaurante(int idRestaurante, int idCiudad, string nombreRest, int numeroAforo, string telefono);
        Task<int> DeleteRestaurante(int id);

        Task<CiudadData> EditCiudad(int id);
        Task<CiudadData> PostCiudad(string nombreCiudad);
        Task<CiudadData> PutCiudad(int id, string nombreCiudad);
        Task<int> DeleteCiudad(int id);
        Task<ResponseToken> CreateToken();
        Task RemoveToken();
    }
}
