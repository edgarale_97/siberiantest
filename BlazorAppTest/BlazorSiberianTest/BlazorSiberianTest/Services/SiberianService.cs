﻿using Blazored.LocalStorage;
using BlazorSiberianTest.AuthProviders;
using BlazorSiberianTest.Models;
using BlazorSiberianTest.Models.Ciudad;
using BlazorSiberianTest.Models.Requests;
using BlazorSiberianTest.Models.Restaurante;
using Microsoft.AspNetCore.Components.Authorization;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Services
{
    public class SiberianService : ISiberianService
    { 
        public readonly HttpClient _httpClient;
        private readonly AuthenticationStateProvider _authStateProvider;
        private readonly ILocalStorageService _localStorage;
        public SiberianService(HttpClient httpClient, AuthenticationStateProvider authenticationStateProvider, ILocalStorageService localStorageService)
        {
            _httpClient = httpClient;
            _authStateProvider = authenticationStateProvider;
            _localStorage = localStorageService;
        }

        public async Task<IEnumerable<CiudadData>> GetAllCiudades()
        {
            var result = JsonConvert.DeserializeObject<ResponseCiudad>(
            await _httpClient.GetStringAsync($"v1/Ciudad"));
            
            return result.Data;

        }

        public async Task<IEnumerable<RestauranteData>> GetAllRestaurantes()
        {
            SpRequest spRequest = new SpRequest
            {
                IdCiudad = 0,
                IdRestaurante = 0,
                NombreRestaurante = "",
                NumeroAforo = 0,
                Telefono = "",
                Tipo = "i"
            };
            var jsonContent = JsonConvert.SerializeObject(spRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            
            var httpResponse = await _httpClient.PostAsync($"v1/Restaurante/ExecuteSp", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseRestaurante>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return new List<RestauranteData>();
        }

        public async Task<RestauranteData> GetRestaurante(int id)
        {
            SpRequest spRequest = new SpRequest
            {
                IdCiudad = 0,
                IdRestaurante = id,
                NombreRestaurante = "",
                NumeroAforo = 0,
                Telefono = "",
                Tipo = "ii"
            };
            var jsonContent = JsonConvert.SerializeObject(spRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var httpResponse = await _httpClient.PostAsync($"v1/Restaurante/ExecuteSp", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseRestaurante>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data[0];
            }

            return new RestauranteData();
        }

        public async Task<IEnumerable<RestauranteData>> PostRestaurante(int idCiudad, string nombreRestaurante, int numeroAforo, string telefono)
        {
            SpRequest spRequest = new SpRequest
            {
                IdCiudad = idCiudad,
                IdRestaurante = 0,
                NombreRestaurante = nombreRestaurante,
                NumeroAforo = numeroAforo,
                Telefono = telefono,
                Tipo = "iii"
            };
            var jsonContent = JsonConvert.SerializeObject(spRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var httpResponse = await _httpClient.PostAsync($"v1/Restaurante/ExecuteSp", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseRestaurante>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return new List<RestauranteData>();
        }

        public async Task<IEnumerable<RestauranteData>> PutRestaurante(int idRestaurante, int idCiudad, string nombreRest, int numeroAforo, string telefono)
        {
            SpRequest spRequest = new SpRequest
            {
                IdCiudad = idCiudad,
                IdRestaurante = idRestaurante,
                NombreRestaurante = nombreRest,
                NumeroAforo = numeroAforo,
                Telefono = telefono,
                Tipo = "iv"
            };
            var jsonContent = JsonConvert.SerializeObject(spRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var httpResponse = await _httpClient.PostAsync($"v1/Restaurante/ExecuteSp", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseRestaurante>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return new List<RestauranteData>();
        }

        public async Task<int> DeleteRestaurante(int id)
        {
            SpRequest spRequest = new SpRequest
            {
                IdCiudad = 0,
                IdRestaurante = id,
                NombreRestaurante = "",
                NumeroAforo = 0,
                Telefono = "",
                Tipo = "v"
            };
            var jsonContent = JsonConvert.SerializeObject(spRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            var httpResponse = await _httpClient.PostAsync($"v1/Restaurante/ExecuteSp", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseRestaurante>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data[0].Id;
            }

            return 0;
        }

        public async Task<CiudadData> EditCiudad(int id)
        {
            var result = JsonConvert.DeserializeObject<ResponseCiudadSingle>(
                await _httpClient.GetStringAsync($"v1/Ciudad/{id}"));

            return result.Data;
        }

        public async Task<CiudadData> PostCiudad(string nombreCiudad)
        {

            CiudadCreateRequest ciudadRequest = new CiudadCreateRequest
            {
                NombreCiudad = nombreCiudad
            };
            var jsonContent = JsonConvert.SerializeObject(ciudadRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            var httpResponse = await _httpClient.PostAsync($"v1/Ciudad", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseCiudadSingle>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return new CiudadData();
        }

        public async Task<CiudadData> PutCiudad(int id, string nombreCiudad)
        {

            CiudadUpdateRequest ciudadRequest = new CiudadUpdateRequest
            {
                IdCiudad = id,
                NombreCiudad = nombreCiudad
            };
            var jsonContent = JsonConvert.SerializeObject(ciudadRequest);
            var httpContent = new StringContent(jsonContent, Encoding.UTF8, "application/json");
            var httpResponse = await _httpClient.PutAsync($"v1/Ciudad/{id}", httpContent);
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseCiudadSingle>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return new CiudadData();
        } 
        
        public async Task<int> DeleteCiudad(int id)
        {

           
            var httpResponse = await _httpClient.DeleteAsync($"v1/Ciudad/{id}");
            if (httpResponse.Content != null)
            {
                // Error Here
                var responseContent = JsonConvert.DeserializeObject<ResponseDelete>(await httpResponse.Content.ReadAsStringAsync());

                return responseContent.Data;
            }

            return 0;
        }

        public async Task RemoveToken()
        {
            await _localStorage.RemoveItemAsync("authToken");
            ((AuthStateProvider)_authStateProvider).NotifyUserLogout();
            _httpClient.DefaultRequestHeaders.Authorization = null;
        }

        public async Task<ResponseToken> CreateToken()
        {
            TokenRequest tokenRequest = new TokenRequest
            {
                Email = "superadmin@gmail.com",
                Password = "123Pa$$word!"
            };
            var content = JsonConvert.SerializeObject(tokenRequest);
            var bodyContent = new StringContent(content, Encoding.UTF8, "application/json");

            var authResult = await _httpClient.PostAsync($"Account/authenticate", bodyContent);
            var authContent = await authResult.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<ResponseToken>(authContent);
            if (!authResult.IsSuccessStatusCode)
                return result;
            await _localStorage.SetItemAsync("authToken", result.Data.JwToken);
            ((AuthStateProvider)_authStateProvider).NotifyUserAuthentication(tokenRequest.Email);
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", result.Data.JwToken);
            return result;
        }
    }
}
