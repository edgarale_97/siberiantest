﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Models.Ciudad
{
    public class CiudadData
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("nombreCiudad")]
        [Required, MaxLength(150)]
        public string NombreCiudad { get; set; }
    }
}
