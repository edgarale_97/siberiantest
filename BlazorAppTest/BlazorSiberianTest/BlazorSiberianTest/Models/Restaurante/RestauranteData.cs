﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Models.Restaurante
{
    public class RestauranteData
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("idCiudad")]
        [Required]
        public int IdCiudad { get; set; }

        [JsonProperty("nombreCiudad")]
        public string NombreCiudad { get; set; } 

        [JsonProperty("nombreRestaurante")]
        [Required, MaxLength(300)]
        public string NombreRestaurante { get; set; }

        [JsonProperty("numeroAforo")]
        [Required]
        public int NumeroAforo { get; set; }

        [JsonProperty("telefono")]
        [Required, MaxLength(100)]
        public string Telefono { get; set; } 

    }
}
