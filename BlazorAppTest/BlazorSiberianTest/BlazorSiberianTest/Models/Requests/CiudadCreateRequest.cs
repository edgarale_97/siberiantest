﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Models.Requests
{
    public class CiudadCreateRequest
    {
        public string NombreCiudad { get; set; }
    }
}
