﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Models.Requests
{
    public class CiudadUpdateRequest
    {
        public string NombreCiudad { get; set; }
        public int IdCiudad { get; set; }
    }
}
