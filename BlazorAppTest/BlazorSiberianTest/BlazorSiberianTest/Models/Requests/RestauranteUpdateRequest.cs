﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorSiberianTest.Models.Requests
{
    public class RestauranteUpdateRequest
    {
        public string Tipo { get; set; }
        public int IdRestaurante { get; set; }
        public string NombreRestaurante { get; set; }
        public int NumeroAforo { get; set; }
        public int IdCiudad { get; set; }
        public string Telefono { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }
}
